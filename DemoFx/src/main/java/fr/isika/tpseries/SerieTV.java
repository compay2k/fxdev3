package fr.isika.tpseries;

public class SerieTV 
{
	private String titre;
	private int nbSaisons;
	private int annee;
	private String producteur;
	private int note;
	
	public SerieTV()
	{
		
	}
	
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public int getNbSaisons() {
		return nbSaisons;
	}
	public void setNbSaisons(int nbSaisons) {
		this.nbSaisons = nbSaisons;
	}
	public int getAnnee() {
		return annee;
	}
	public void setAnnee(int annee) {
		this.annee = annee;
	}
	public String getProducteur() {
		return producteur;
	}
	public void setProducteur(String producteur) {
		this.producteur = producteur;
	}
	public int getNote() {
		return note;
	}
	public void setNote(int note) {
		this.note = note;
	}
	
	
	
	
}
