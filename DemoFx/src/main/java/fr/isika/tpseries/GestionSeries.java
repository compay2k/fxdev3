package fr.isika.tpseries;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

import fr.isika.demofx.Pays;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;

public class GestionSeries extends BorderPane 
{
	TableView<SerieTV> tbvSeries;

	private ArrayList<SerieTV> chargerListeSeries(String chemin) 
	{
		ArrayList<SerieTV> resultat = new ArrayList<>();

		try {
			// classes de lecture du fichier :
			FileReader fr = new FileReader(chemin);
			BufferedReader br = new BufferedReader(fr);

			String ligne = br.readLine();

			while (ligne != null) {
				// récupérer la serie
				String[] attributs = ligne.split(";");

				SerieTV serie = new SerieTV();

				serie.setTitre(attributs[0]);
				serie.setNbSaisons(Integer.parseInt(attributs[1]));
				serie.setAnnee(Integer.parseInt(attributs[2]));
				serie.setProducteur(attributs[3]);
				serie.setNote(Integer.parseInt(attributs[4]));

				resultat.add(serie);

				// lire la ligne suivante :
				ligne = br.readLine();
			}

			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return resultat;
	}

	private void remplirTable() 
	{
		// remplir le TableView :
		ObservableList<SerieTV> series = FXCollections.observableArrayList(chargerListeSeries("D:/series.csv"));

		tbvSeries = new TableView<SerieTV>(series);

		TableColumn<SerieTV, String> colTitre = new TableColumn<>("Titre");
		colTitre.setCellValueFactory(new PropertyValueFactory<SerieTV, String>("titre"));

		TableColumn<SerieTV, Integer> colSaisons = new TableColumn<>("nb. saisons");
		colSaisons.setCellValueFactory(new PropertyValueFactory<SerieTV, Integer>("nbSaisons"));

		TableColumn<SerieTV, Integer> colAnnee = new TableColumn<>("Année");
		colAnnee.setCellValueFactory(new PropertyValueFactory<SerieTV, Integer>("annee"));

		TableColumn<SerieTV, String> colProd = new TableColumn<>("Producteur");
		colProd.setCellValueFactory(new PropertyValueFactory<SerieTV, String>("producteur"));

		TableColumn<SerieTV, Integer> colNote = new TableColumn<>("Note");
		colNote.setCellValueFactory(new PropertyValueFactory<SerieTV, Integer>("note"));

		tbvSeries.getColumns().addAll(colTitre, colSaisons, colAnnee, colProd, colNote);
	}

	public GestionSeries() 
	{
		remplirTable();
		this.setLeft(tbvSeries);
		
		
	}

}
