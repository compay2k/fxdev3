package fr.isika.tpseries;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class TPSeriesMain extends Application{

	public static void main(String[] args) 
	{
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception 
	{
		GestionSeries root = new GestionSeries();
		
		Scene s = new Scene(root, 700, 700);
		
		primaryStage.setScene(s);
		primaryStage.show();
	}
	
}
