package fr.isika.demofx;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class DemoEvts1 extends Application
{
	public static void main(String[] args) 
	{
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception 
	{
		HBox root = new HBox(30);
		root.setAlignment(Pos.CENTER);
		
		TextField txtNom = new TextField();
		Button btnOk = new Button("Cliquez moi");
		Label lblMessage = new Label();
		
		btnOk.setOnAction
		(
				new EventHandler<ActionEvent>() 
				{
					@Override
					public void handle(ActionEvent event) 
					{
						lblMessage.setText("Bonjour " + txtNom.getText());			
					}
				}
		);
		
		
		root.getChildren().add(txtNom);
		root.getChildren().add(btnOk);
		root.getChildren().add(lblMessage);
		
		Scene s = new Scene(root, 400, 400);
		primaryStage.setScene(s);
		primaryStage.show();
	
	}
	
}
