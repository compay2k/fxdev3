package fr.isika.demofx;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AppFormulaire extends Application{

	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		DemoFormulaire root = new DemoFormulaire();
		
		Scene s = new Scene(root);
		
		primaryStage.setScene(s);
		
		primaryStage.show();
		
	}

}
