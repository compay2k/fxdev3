package fr.isika.demofx;

import java.io.File;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;

public class DemoFormulaire extends GridPane
{
	// liste deroulante :
	private ChoiceBox<String> cbxVilles = new ChoiceBox<>();
	
	// case à cocher :
	private CheckBox chkOptin = new CheckBox("J'accepte de vendre mon âme et toutes mes possessions à Google et recevoir des enwsletters pour le restant de mes jours");
	
	public DemoFormulaire()
	{
		//-------------------------------------- 
		// liste deroulante 
		//--------------------------------------
		// remplissage :
		cbxVilles.getItems().add("Manchester");
		cbxVilles.getItems().addAll("Rennes", "Montpellier", "Chelsea", "Madrid");
		cbxVilles.getItems().add(1, "Barcelone");
		
		// selectionner :
		cbxVilles.getSelectionModel().select(2);
		
		// recuperer l'item choisi :
		cbxVilles.getSelectionModel().getSelectedItem();
		
		cbxVilles.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				System.out.println(cbxVilles.getSelectionModel().getSelectedItem());
				
			}
		});
		
		this.add(new Label("ville : "), 0, 0);
		this.add(cbxVilles, 1, 0);
		
		//-------------------------------------- 
		// Checkbox
		//--------------------------------------
		
		// cocher / decocher :
		chkOptin.setSelected(true);
		
		// vérifier si coché :
		if (chkOptin.isSelected())
		{
			System.out.println("case cochée");
		}
		
		this.add(chkOptin, 0, 1);
		GridPane.setColumnSpan(chkOptin, 2);
		
		//----------------------------
		// boutons radio :
		//----------------------------
		
		RadioButton radSexeM = new RadioButton("Masculin");
		RadioButton radSexeF = new RadioButton("Feminin");
		
		// regroupement :
		ToggleGroup tggSexe = new ToggleGroup();
		tggSexe.getToggles().addAll(radSexeF, radSexeM);
		
		this.add(radSexeF, 0, 2);
		this.add(radSexeM, 1, 2);
		
		// selection d'un item :
		tggSexe.selectToggle(radSexeF);
		
		// récupérer l'item choisi :
		RadioButton boutonChoisi = (RadioButton)tggSexe.getSelectedToggle();
		System.out.println(boutonChoisi.getText());
		
		//------------------------------------
		// File chooser
		//------------------------------------
		
		Button btnFichier = new Button("Parcourir...");
		Label lblChemin = new Label();
		
		this.add(btnFichier, 0, 3);
		this.add(lblChemin, 1, 3);
		
		btnFichier.setOnAction(new EventHandler<ActionEvent>() 
		{
			@Override
			public void handle(ActionEvent event) {
				
				FileChooser fc = new FileChooser();
				
				fc.setInitialDirectory(new File("C:/users/stagiaire"));
				
				File f = fc.showOpenDialog(getScene().getWindow());
				
				if (f != null)
				{
					lblChemin.setText(f.getAbsolutePath());
				}
				else
				{
					lblChemin.setText("Pas de sélection...");
				}
				
			}
		});
		
	}
	
}







