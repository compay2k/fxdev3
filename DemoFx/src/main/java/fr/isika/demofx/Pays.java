package fr.isika.demofx;

public class Pays {

	private String nom;
	private String capitale;
	private String code;
	private int surface;
	private long population;
	
	public Pays()
	{
		
	}
	
	public Pays(String nom, String capitale, String code, int surface, long population) {
		super();
		this.nom = nom;
		this.capitale = capitale;
		this.code = code;
		this.surface = surface;
		this.population = population;
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getCapitale() {
		return capitale;
	}
	public void setCapitale(String capitale) {
		this.capitale = capitale;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public int getSurface() {
		return surface;
	}
	public void setSurface(int surface) {
		this.surface = surface;
	}
	public long getPopulation() {
		return population;
	}
	public void setPopulation(long population) {
		this.population = population;
	}
	
	
}
