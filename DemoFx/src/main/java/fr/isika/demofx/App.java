package fr.isika.demofx;

import javafx.application.*;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class App extends Application{

	public static void main(String[] args) 
	{
		System.out.println("je suis dans le main");
		launch(args);

	}

	@Override
	public void start(Stage stage) throws Exception {
		//On instancie un AnchorPane:
		AnchorPane root = new AnchorPane();


		//On instancie un pannel que l'on stylise:
		Pane pannel = new Pane();

		pannel.setStyle("-fx-background-color: teal");
		//On ajoute le pannel à la liste de Nodes enfants de l'AnchorPane

		root.getChildren().add(pannel);	

		//On définit les ancres de chaque côté:
		AnchorPane.setTopAnchor(pannel, 50.);
		AnchorPane.setBottomAnchor(pannel, 10.);
		AnchorPane.setLeftAnchor(pannel, 10.);
		AnchorPane.setRightAnchor(pannel, 10.);
		
		//On instancie notre scène en lui donnant notre pannel root et des dimensions:
		Scene scene = new Scene(root, 400, 400);
		
		//On donne un titre à notre stage:
		stage.setTitle("Premier AnchorPane");
		//On donne à notre stage notre scène:
		stage.setScene(scene);
		
		//On affiche notre stage
		stage.show();
	}

	public void start5(Stage stg) throws Exception
	{
		BorderPane root = new BorderPane();
		
		Pane p1 = new Pane();
		p1.setStyle("-fx-background-color:bisque;");
		p1.setPrefHeight(100);
		
		Label lblTruc = new Label("Truc");
		lblTruc.relocate(42, 42);
		p1.getChildren().add(lblTruc);
		
		Pane p2 = new Pane();
		p2.setStyle("-fx-background-color:olive;");
		p2.setPrefWidth(75);
		
		Pane p3 = new Pane();
		p3.setStyle("-fx-background-color:hotpink;");
		
		root.setTop(p1);
		root.setLeft(p2);		
		root.setCenter(p3);
		
		Scene s = new Scene(root, 600, 400);
		stg.setScene(s);
		stg.show();
	}
	
	
	public void start4(Stage stg) throws Exception
	{
		GridPane root = new GridPane();
		
		// bordure
		root.setPadding(new Insets(30));
		// marges interieures
		root.setVgap(10); // lignes
		root.setHgap(10); // colonnes
		
		Label lblNom = new Label("Votre nom : ");
		TextField txtNom = new TextField();
		txtNom.setPromptText("ex : Toto");
		
		Label lblPrenom = new Label("prénom : ");
		TextField txtPrenom = new TextField();
		
		Label lblAge = new Label("age : ");
		TextField txtAge = new TextField();
		
		Button btnOk = new Button("OK je veux créer un compte et recevoir des newsletters pour le restant de mes jours");
		
		root.add(lblNom, 0, 0);
		root.add(txtNom, 1, 0);
		root.add(lblPrenom, 2, 0);
		root.add(txtPrenom, 3, 0);
		root.add(lblAge, 0, 1);
		root.add(txtAge, 1, 1);
		root.add(btnOk, 2, 1);
		
		root.setColumnSpan(btnOk, 3);
		
		Scene s = new Scene(root,400,400);
		
		stg.setScene(s);
		stg.show();
	}
	
	
	public void start2(Stage stg) throws Exception
	{	
		//VBox root = new VBox(10);
		FlowPane root = new FlowPane(10,20);
		root.setPadding(new Insets(25));
		
		root.setAlignment(Pos.CENTER);
		root.setOrientation(Orientation.VERTICAL);
		
		Scene s = new Scene(root, 400, 400);

		for (int i = 0; i < 25; i++) 
		{
			Button btn = new Button("Button " + i);
			root.getChildren().add(btn);
		}
		stg.setScene(s);
		stg.show();
	}
	
	
	public void start1(Stage primaryStage) throws Exception 
	{
		// je crée le panel racine de ma fenetre :
		Pane p = new Pane(); 
		p.setStyle("-fx-background-color:hotpink");
		
		// je crée un cercle :
		Circle cercle = new Circle(75);
		// je colore mon cercle :
		cercle.setFill(Color.YELLOW);
		cercle.setStroke(Color.ORANGE);		
		// je positionne mon cercle :
		cercle.relocate(200, 200);
				
		
		Label lbl = new Label();
		lbl.setText("Plop");
		lbl.setTextFill(Color.BLACK);
		lbl.setFont(Font.font("Courier new", FontWeight.BOLD ,20));
		lbl.relocate(217, 217);
		
		
		Button b1 = new Button("Ne pas cliquer par pitié  qshd jhqskd kqhsk dhqkshd khq ks'il vous plait");
		b1.relocate(10, 10);
		
		Button b2 = new Button("Cliquez moi !!");
		b2.relocate(250, 10);
		
		p.getChildren().add(b1);
		p.getChildren().add(b2);
		p.getChildren().add(cercle);
		p.getChildren().add(lbl);
		
		// je définis une fenetre :
		Scene s = new Scene(p, 500, 350);
		
		// j'associe ma fenetre(scene) au stage courant :
		primaryStage.setScene(s);
		
		primaryStage.setTitle("HELLO FX");
		
		// affichage de la fenetre
		primaryStage.show();		
	
	}

}
