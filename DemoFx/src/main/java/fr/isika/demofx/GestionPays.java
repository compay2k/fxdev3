package fr.isika.demofx;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class GestionPays extends BorderPane
{
	private TableView<Pays> listePays;
	
	private TextField txtNom = new TextField();
	private Button btnEnregistrer = new Button("Enregistrer");
	
	public GestionPays()
	{
		HBox formEdition = new HBox(30);
		formEdition.setAlignment(Pos.CENTER);
		formEdition.setPadding(new Insets(20));
		
		formEdition.getChildren().addAll(txtNom, btnEnregistrer);
		
		this.setCenter(formEdition);
		
		// je créee une liste "normale" :
		List<Pays> countries = new ArrayList<>();
		countries.add(new Pays("Mongolie", "Ulan Bator", "MNG", 356000, 3000000));
		countries.add(new Pays("Perou", "Lima", "PER", 56445654, 32468005));
		countries.add(new Pays("Ethiopie", "Addis Abeba", "ETH", 7645100, 640000));
		countries.add(new Pays("Khazakstan", "Astana", "KHA", 765450, 9500450));
		
		// je crée une liste observable à partir de ma liste normale :
		ObservableList<Pays> donnees = FXCollections.observableArrayList(countries);
		
		// je déclare mon tableView :
		listePays = new TableView<>(donnees);
		listePays.setMinWidth(350);
		
		TableColumn<Pays, String> colNom = new TableColumn<>("Nom du pays");
		colNom.setCellValueFactory(new PropertyValueFactory<Pays, String>("nom"));
		
		TableColumn<Pays, Long> colPop = new TableColumn<>("Population");
		colPop.setCellValueFactory(new PropertyValueFactory<Pays, Long>("population"));
		
		listePays.getColumns().addAll(colNom, colPop);
		
		this.setLeft(listePays);
		
		// Détection d'un nouveau choix dans la liste :
		listePays.getSelectionModel()
				 .selectedItemProperty()
				 .addListener(new ChangeListener<Pays>() 
				 {
					@Override
					public void changed(ObservableValue<? extends Pays> observable, 
										Pays oldValue, 
										Pays newValue) 
					{
						txtNom.setText(newValue.getNom());
					}
				});
		
		// Enregistrement des modifications :
		btnEnregistrer.setOnAction(new EventHandler<ActionEvent>() 
		{
			@Override
			public void handle(ActionEvent event) {
				// récupérer le pays sélectionné :
				Pays p = listePays.getSelectionModel().getSelectedItem();
				p.setNom(txtNom.getText());
				listePays.refresh();
			}
		});
		
		
		//countries.get(3).setNom("Kaz");
				
	}
}





