package fr.isika.tpfx;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

public class MenuRouge extends HBox
{
	private Button b1;
	private Button b2;
	private Button b3;
	private Button b4;
	
	public MenuRouge()
	{
		this.setSpacing(30);
		this.setAlignment(Pos.CENTER);
		this.setStyle("-fx-background-color:red;");
		this.setMinHeight(100);
		
		b1 = new Button("bouton 1");
		b2 = new Button("bouton 2");
		b3 = new Button("bouton 3");
		b4 = new Button("bouton 4");
		
		this.getChildren().add(b1);
		this.getChildren().add(b2);
		this.getChildren().add(b3);
		this.getChildren().add(b4);
	}
}
