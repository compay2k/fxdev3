package fr.isika.tpfx;

import java.text.DecimalFormat;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class CalculetteApp extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	private double operande = 0;
	private String operateur;
	private boolean aRenouveler = false;
	
	@Override
	public void start(Stage primaryStage) throws Exception {

		GridPane root = new GridPane();

		String[][] boutons = {{"1","2","3","+"},
								{"4","5","6","-"},
								{"7","8","9","*"},
								{"0","=","/"}};
		
		TextField txtAffichage = new TextField();
		Button btnAnnuler = new Button("C");
		
		btnAnnuler.setMinWidth(80);
		btnAnnuler.setMinHeight(80);
		txtAffichage.setMinHeight(80);
		txtAffichage.setFont(new Font(25));

		// ligne 1 :
		root.add(txtAffichage, 0, 0);
		GridPane.setColumnSpan(txtAffichage, 3);
		root.add(btnAnnuler, 3, 0);
		
		btnAnnuler.setOnAction(new EventHandler<ActionEvent>() 
		{
			@Override
			public void handle(ActionEvent event) {
				txtAffichage.clear();
				operande = 0;				
			}
		});
		
		for (int i = 0; i < boutons.length; i++)
		{
			for (int j = 0; j < boutons[i].length; j++)
			{
				String nom = boutons[i][j];
				Button btn = new Button(nom);
				btn.setMinWidth(80);
				btn.setMinHeight(80);
				
				if (nom.equals("="))
				{
					root.add(btn, j, i + 1);
					GridPane.setColumnSpan(btn, 2);
					btn.setMinWidth(2 * btn.getMinWidth());
				}
				else if (nom.equals("/"))
				{
					root.add(btn, j + 1, i + 1);
				}
				else
				{
					root.add(btn, j, i + 1);
				}
				
				btn.setOnAction(new EventHandler<ActionEvent>() 
				{
					@Override
					public void handle(ActionEvent event) 
					{
						String valeur = btn.getText();

						switch(valeur)
						{
							case "+" :
							case "-" :
							case "*" :
							case "/" :
								operande = Double.parseDouble(txtAffichage.getText());
								operateur = valeur;
								txtAffichage.clear();
								break;
							case "=" :
								aRenouveler = true;
								double resultat = 0;
								double a = operande;
								double b = Double.parseDouble(txtAffichage.getText());
								switch(operateur)
								{
									case "+":
										resultat = a + b;
										break;
									case "-":
										resultat = a - b;
										break;
									case "*":
										resultat = a * b;
										break;
									case "/":
										resultat = a / b;
										break;
								}
								
								DecimalFormat df = new DecimalFormat("#.##########");
								txtAffichage.setText(df.format(resultat)); 
								
								break;
							default :
								// Boutons des chiffres :
								if (aRenouveler)
								{
									txtAffichage.setText(valeur);
									aRenouveler = false;
								}
								else
								{
									txtAffichage.setText(txtAffichage.getText() + valeur);
								}
						}
					}
				});
				
			}
		}
		
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();

	}

}
