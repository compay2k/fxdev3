package fr.isika.tpfx;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class MenuVert extends VBox 
{
	private Label l1;
	private Label l2;
	private Label l3;
	private Label l4;
	
	public MenuVert()
	{
		this.setSpacing(40);
		this.setAlignment(Pos.CENTER);
		this.setStyle("-fx-background-color:green;");
		this.setMinWidth(100);
		
		l1 = new Label("Label 1");
		l2 = new Label("Label 2");
		l3 = new Label("Label 3");
		l4 = new Label("Label 4");
		
		this.getChildren().add(l1);
		this.getChildren().add(l2);
		this.getChildren().add(l3);
		this.getChildren().add(l4);
	}
	
}
