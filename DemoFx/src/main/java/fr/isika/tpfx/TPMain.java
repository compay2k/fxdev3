package fr.isika.tpfx;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TPMain extends Application
{

	public static void main(String[] args) 
	{
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception 
	{
		BorderPane root  = new BorderPane();	
		
		MenuRouge zoneHaut = new MenuRouge();
		
		MenuVert zoneGauche = new MenuVert();
		
		Pane zoneMilieu = new Pane();
		zoneMilieu.setStyle("-fx-background-color:turquoise;");
		
		root.setTop(zoneHaut);
		root.setLeft(zoneGauche);
		root.setCenter(zoneMilieu);		
		
		Scene s = new Scene(root, 600, 400);
		primaryStage.setScene(s);
		primaryStage.show();
	}

}
